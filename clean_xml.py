#!/usr/bin/env python3

"""
Clean xml
    - clean text
    - clean new line
    - remove duplicated sequence label
"""

from lxml import etree
import pandas as pd
import time
import sys
import os
import re


def getDirectoryList(path):
    """
    return a list of all directories which contain at least one .xml file.
    """
    directoryList = []

    #return nothing if path is a file
    if os.path.isfile(path):
        return []

    #add dir to directorylist if it contains .txt files
    if os.path.isdir(path):
        if len([f for f in os.listdir(path) if f.endswith('.xml')])>0:
            directoryList.append(path)

        for d in os.listdir(path):
            new_path = os.path.join(path, d)
            if os.path.isdir(new_path):
                directoryList += getDirectoryList(new_path)

    return directoryList


def clean_text(rootdir, outdir):
    """
    Go through text files in rootdir
        - pass 1: fixing repeating amps: &amp;amp;...amp;#digits; --> &#digits;
             - i.e. don&amp;amp;amp;#39;t --> don&#39;t
        - pass 2: fixing repeating amps: &amp;amp;...amp; --> &amp;
             - i.e. A&amp;amp;amp;E --> A&amp;E
        - pass 3: special case &#160: --> &#160;
        - pass 4: special case &#163< --> &#163;<
    """

    try:
        files = [os.path.basename(f) for f in os.listdir(rootdir) if os.path.isfile(os.path.join(rootdir, f))]
    except WindowsError:
        print("something is wrong")
        sys.exit(1)

    for filename in files:
        filename = os.path.join(rootdir, filename)
        print(filename + r": pass 1 fixing repeat amps: '&amp;amp;...amp;#xx;' -> '&#xx;")

        tmpfile1 = filename + ".temp1"
        tmpfile2 = filename + ".temp2"
        tmpfile3 = filename + ".temp3"

        with open(filename, "r") as fin:
            with open(tmpfile1, "w") as fout:
                for line in fin:
                    # regex: at least one 'amp;' in \1 and digits in \2
                    # replace all amp; right in front of a digit with only one &
                    # i.e. don&amp;amp;amp;#39;t --> don&#39;t
                    fout.write(re.sub(r'\&(amp;)+#(\d+);', r'&#\2;', line))

        print(filename + ": pass 2 fixing multiple '&amp;amp;amp;amp;'")
        with open(tmpfile1, "r") as fin:
            with open(tmpfile2, "w") as fout:
                for line in fin:
                    # A&amp;amp;E --> A&amp;E
                    fout.write(re.sub(r'\&(amp;)+', r'&amp;', line))

        print(filename + ": pass 3 fixing '&#160:' (note colon)")
        with open(tmpfile2, "r") as fin:
            with open(tmpfile3, "w") as fout:
                for line in fin:
                    # &#160: with &#160;
                    fout.write(re.sub(r"(&#[0-9]+):", r"\1;", line))

        print(filename + r": pass 4 fixing '&#163<' (note < tag start")
        with open(tmpfile3, "r") as fin:
            # note overwrite the original file
            with open(os.path.join(outdir, os.path.basename(filename)), "w") as fout:
                for line in fin:
                    fout.write(line.replace("&#163<", "&#163;<"))

        # remove tmp
        print(filename + ": deleting tmpfile")
        os.unlink(tmpfile1)
        os.unlink(tmpfile2)
        os.unlink(tmpfile3)


def clean_newline(rootdir):
    """
    Overwrite the original xml file with a middle of context line break replaced by a space.
    Also expand the escaped characters, for example: &#163; becomes £
    Line breaks in text are generally represented as:
        \r\n - on a windows computer
        \r   - on an Apple computer
        \n   - on Linux
    """

    try:
        files = [f for f in os.listdir(rootdir) if os.path.isfile(os.path.join(rootdir, f))]
    except WindowsError:
        print("something is wrong")
        sys.exit(1)

    for filename in files:
        filename = os.path.join(rootdir, filename)
        print(filename)
        p = etree.XMLParser(resolve_entities=True)
        with open(filename, "rt") as f:
            tree = etree.parse(f, p)

        for node in tree.iter():
            if node.text is not None:
                if re.search("\n|\r|\r\n", node.text.rstrip()):
                    node.text = node.text.replace("\r\n", " ")
                    node.text = node.text.replace("\r", " ")
                    node.text = node.text.replace("\n", " ")

        # because encoding="UTF-8" in below options, the output can contain non-ascii characters, e.g. £
        tree.write(filename, encoding="UTF-8", xml_declaration=True)


def rm_duplicate_n(rootdir):
    """
    Remove text <duplicate [n]> from sequence labels in the XML after it has exported
    """

    try:
        files = [f for f in os.listdir(rootdir) if os.path.isfile(os.path.join(rootdir, f))]
    except WindowsError:
        print("something is wrong")
        sys.exit(1)

    for filename in files:
        # get file name and extension
        (only_name, only_extension) = os.path.splitext(filename)

        # adding the new name with extension
        new_base = only_name + '_duplicates' + only_extension
        old_file_location = os.path.join(rootdir, filename)
        new_file_location = os.path.join(rootdir, new_base)

        p = etree.XMLParser(resolve_entities=True)
        with open(old_file_location, "rt") as f:
            tree = etree.parse(f, p)

        check = []
        for node in tree.iter():
            if node.text is not None:
                check_duplicate = bool(re.search(r" duplicate \d+$", node.text))
                check.append(check_duplicate)
                node.text = re.sub(r" duplicate \d+$", "", node.text)

        # if contains duplicates
        if any(check) is True:
            # rename to prefix_duplicates
            os.rename(old_file_location, new_file_location)
            # write removed duplicates file to original name
            tree.write(old_file_location, encoding="UTF-8", xml_declaration=True)


def clean_xml(input_dir, output_dir):
    """
    Clean downloaded xml files
    """
    print("clean text")
    clean_text(input_dir, output_dir)
    print("clean new line")
    clean_newline(output_dir)
    print("rm duplicate n from sequence label")
    rm_duplicate_n(output_dir)


def main():

    # clean xml
    out_file = 'export_xml'
    xml_dir_list = getDirectoryList(out_file)

    for xml_dir in xml_dir_list:
        clean_xml_dir = xml_dir + '_clean'
        if not os.path.exists(clean_xml_dir):
            os.makedirs(clean_xml_dir)
            clean_xml(xml_dir, clean_xml_dir)


if __name__ == "__main__":
    main()


