## Export xml from archivist

*Note*: this is the step 2 from [create_new_export](https://gitlab.com/closer-cohorts1/archivist_export/create_new_export), i.e. no `get the latest version` step.


### Input
List of prefix: Prefixes_to_export.txt

### Output
- export_xml directory
    - xml_list.csv:
        - prefix
        - xml_date
        - xml_location
    - archivist_instance directory
        - raw xml files
    - archivist_instance_clean directory
        - cleaned xml files

### Data cleaning step
- Clean text
    - pass 1: fixing repeating amps: ```&amp;amp;...amp;#digits; --> &#digits;```
         - i.e. ```don&amp;amp;amp;#39;t --> don&#39;t```
    - pass 2: fixing repeating amps: ```&amp;amp;...amp; --> &amp;```
         - i.e. ```A&amp;amp;amp;E --> A&amp;E```
    - pass 3: special case ```&#160: --> &#160;```
    - pass 4: special case ```&#163< --> &#163;<```

- Remove text <duplicate [n]> from sequence labels
